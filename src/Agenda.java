import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Agenda {

    List<Contato> contatos = new ArrayList<>();


    public void mostrarMenu() {
        int opcao;
        do {
            String opcaoInput = JOptionPane.showInputDialog(null, "1 - Cadastrar contato \n2 - Mostrar contatos \n3 - Consultar contato por telefone \n4 - Consultar contato por email \n5 - Excluir contato por email \n 6 - Sair");
            opcao = Integer.parseInt(opcaoInput);
            if (opcao == 1) {
                this.solicitarQuantidadeContatos();
            } else if (opcao == 2) {
                this.mostrarContatos();
            } else if(opcao == 3) {
                this.mostrarContatoPorNumero();
            } else if (opcao == 4) {
                this.mostrarContatoPorEmail();
            } else if (opcao == 5) {
                this.removerContatoPorEmail();
            } else if (opcao > 6) {
                JOptionPane.showMessageDialog(null, "Opção inválida, tente novamente!");
            }
        } while (opcao != 6);
    }

    public void solicitarQuantidadeContatos() {
        String qntdContatos = JOptionPane.showInputDialog(null, "Digite quantos contatos deseja adicionar");
        try {
            this.cadastrarContato(Integer.parseInt(qntdContatos));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Você tem que inserir um número");
        }
    }

    public void cadastrarContato(int qntdContatos) {
        for (int i = 0; i < qntdContatos; i++) {
            String nome = JOptionPane.showInputDialog(null, "Digite o nome:");
            String telefone = JOptionPane.showInputDialog(null, "Digite o telefone:");
            String email = JOptionPane.showInputDialog(null, "Digite o email:");

            Contato contato = new Contato(nome, telefone, email);

            this.contatos.add(contato);

            JOptionPane.showMessageDialog(null, "Contato adicionado com sucesso!");
        }

    }

    public void mostrarContatos() {
        String resposta = "";
        int aux = 1;
        for (Contato contato: this.contatos) {
            resposta = resposta + "\n" + aux + " - " + contato.getNome();
        }
        JOptionPane.showMessageDialog(null, resposta);
    }

    public void mostrarContatoPorEmail() {
        boolean foiEncontrado = false;
        String email = JOptionPane.showInputDialog(null, "Digite o email do contato");
        for (Contato contato: contatos) {
            if (contato.getEmail().contains(email)) {
                foiEncontrado = true;
                JOptionPane.showMessageDialog(null, "\nContato \nNome: " + contato.getNome() + "\nTelefone: " + contato.getTelefone() + "\nEmail: " + contato.getEmail());
            }
        }
        if (!foiEncontrado) {
            JOptionPane.showMessageDialog(null, "Desculpe, mas não encontramos nenhum email com esse nome.");
        }
    }

    public void mostrarContatoPorNumero() {
        boolean foiEncontrado = false;
        String numero = JOptionPane.showInputDialog(null, "Digite o número de telefone do contato");
        for (Contato contato: contatos) {
            if (contato.getTelefone().contains(numero)) {
                foiEncontrado = true;
                JOptionPane.showMessageDialog(null, "\nContato \nNome: " + contato.getNome() + "\nTelefone: " + contato.getTelefone() + "\nEmail: " + contato.getEmail());
            }
        }
        if (!foiEncontrado) {
            JOptionPane.showMessageDialog(null, "Desculpe, mas não encontramos nenhum telefone com esse número");
        }
    }

    public void removerContatoPorEmail() {
        boolean foiEncontrado = false;
        String email = JOptionPane.showInputDialog(null, "Digite o email do contato que deseja excluir");
        for (Contato contato: contatos) {
            if (contato.getEmail().contains(email)) {
                foiEncontrado = true;
                int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir esse contato?"
                        + "\nNome: " + contato.getNome()
                        + "\nTelefone: " + contato.getTelefone()
                        + "\nEmail: " + contato.getEmail()
                );
                if (opcao == 0) {
                    JOptionPane.showMessageDialog(null, "Contato excluído com sucesso!");
                }
            }
        }
        if (!foiEncontrado) {
            JOptionPane.showMessageDialog(null, "Desculpe, mas não encontramos nenhum email com esse nome");
        }
    }


}


